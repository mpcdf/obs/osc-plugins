import time
import mpcdf_common

import os
import osc
import osc.conf
import osc.core
import osc.cmdln

from xml.etree import ElementTree


@osc.cmdln.option('-b', "--batch", action="store_true",
                  help="do not ask for confirmation")
@osc.cmdln.option('-m', "--message", nargs=1,
                  help="message to include in the submit request")
def do_mpcdf_push(self, subcmd, opts, *args):
    """mpcdf_push:
    Push packages from a home project to a general project

    You would typically use this to push your home project package into the
    "software" project.

    If the package does not yet exist on the target project, it is created
    there and your source project is connected to that as a branch (by using
    the osc mpcdf_set_as_branch command)

    Usage:
        osc mpcdf_push ["software"]         (from within a package checkout)
        osc mpcdf_push PACKAGE ["software"] (from within a project checkout)
        osc mpcdf_push FROM_PROJECT PACKAGE ["software"]          (anywhere)

    Examples:
       - Add a new package from your home:project to software:
         #> osc mpcdf_push home:$USER newpkg_1_2 software

    ${cmd_option_list}
    """
    import sys

    if len(args) > 0 and args[-1] == "software":
        print("Deprecation Notice:\n"
              "  'osc mpcdf_push' can only be used to push to the 'software' project,\n"
              "  it is thus no longer necessary as an argument\n")
        args = args[:-1]

    to_project = "software"

    if len(args) == 0:
        if osc.core.is_package_dir(os.curdir):
            package = osc.core.store_read_package(os.curdir)
            from_project = osc.core.store_read_project(os.curdir)
        else:
            raise osc.oscerr.WrongArgs('Specify FROM_PROJECT and/or PACKAGE or run command in a package or project checkout directory')
    elif len(args) == 1:
        package, = args
        if osc.core.is_project_dir(os.curdir):
            from_project = osc.core.store_read_project(os.curdir)
        else:
            raise osc.oscerr.WrongArgs('Specify FROM_PROJECT or run command in an osc package or project checkout directory')
    elif len(args) == 2:
        from_project, package = args
    else:
        raise osc.oscerr.WrongArgs("Too many arguments")

    if not from_project.startswith("home:"):
        raise osc.oscerr.WrongArgs("'osc mpcdf_push' can only be used from packages in a home: project")

    api_url = self.get_api_url()

    try:
        existing_maintainers = mpcdf_common.maintainers(api_url, to_project, package)
    except osc.core.HTTPError as e:
        if e.code == 404:
            existing_maintainers = set()
        else:
            raise e

    if not mpcdf_common.has_attribute(api_url, from_project, package, "MPCDF:enable_repositories"):
        print("ERROR: Package is unmanaged.\n"
              "Configure the enabled repositories first, with `osc mpcdf_enable_repositories`", file=sys.stderr)
        raise SystemExit(1)

    req_id = None
    try:
        req_id = osc.core.create_submit_request(api_url, from_project, package, dst_project=to_project,
                                                message="Update {0} from {1}".format(package, from_project) if not opts.message else opts.message)
    except osc.core.HTTPError as e:
        if e.code == 400:
            if "The request contains no actions" in mpcdf_common.decode_it(e.read()):
                print("No source changes found, submit request not necessary")
        else:
            raise e

    if req_id:
        print("Submitted package {0} to {1} in request {2}".format(package, to_project, req_id))
        if not opts.batch:
            result = osc.core.change_request_state(api_url, req_id, 'accepted', "Accepted on the command line via 'osc mpcdf_push'")
            print("  Accepting request {0}:".format(req_id), result)

    if not opts.batch:
        user = osc.conf.get_apiurl_usr(api_url)
        if user not in existing_maintainers:
            print("Adding you to the list of maintainers for this package")
            to_meta = mpcdf_common.package_meta(api_url, to_project, package)
            person = ElementTree.Element("person")
            person.set("userid", user)
            person.set("role", "maintainer")
            to_meta.insert(2, person)
            osc.core.edit_meta("pkg", (to_project, package), data=ElementTree.tostring(to_meta), apiurl=api_url)
        print()

    # Give the system some time, sadly there is no transactional guarantee
    time.sleep(2)

    url = osc.core.makeurl(api_url, ["source", to_project])
    entries = ElementTree.fromstringlist(osc.core.streamfile(url, osc.core.http_GET))

    if entries.find('./entry[@name="{0}"]'.format(package)) is not None:
        for attribute in mpcdf_common.package_attributes + mpcdf_common.config_attributes:
            if mpcdf_common.has_attribute(api_url, from_project, package, attribute):
                print("Setting attribute", attribute)
                attr = mpcdf_common.get_attribute(api_url, from_project, package, attribute)
                mpcdf_common.set_attribute(api_url, to_project, package, attr)

        if not mpcdf_common.mpcdf_enable_repositories(api_url, to_project, package):
            print("ATTENTION: Not changing unmanaged package {0}".format(package))

        filelist = osc.core.meta_get_filelist(api_url, from_project, package)
        if "_link" not in filelist:
            print("Setting branch relationship")
            mpcdf_common.set_as_branch(api_url, from_project, package, to_project, package)

    mpcdf_common.sync_projects(api_url, package)
