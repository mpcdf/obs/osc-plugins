#!/usr/bin/python3

import mpcdf_common

import os
import osc
import osc.conf
import osc.core

try:
    from termcolor import colored
except ImportError:
    def colored(text, *args):
        return text
from xml.etree import ElementTree


@osc.cmdln.option('--skip-home', action="store_true",
                  help="do not show the build status in the user's home project; will be ignored if PROJECT is given")
@osc.cmdln.option('--ignore', metavar="STATUSES",
                  help="ignore some build statuses, where STATUSES is a comma-separated list (for example, 'scheduled,unresolvable')")
@osc.cmdln.option('--count', action="store_true",
                  help="display the number of builds for each status")
@osc.cmdln.option('--only-count', action="store_true",
                  help="only display the number of builds for each status (print nothing else)")
@osc.cmdln.option('--details', metavar="FILTER",
                  help="print details; set FILTER to '*' to print all; set FILTER to any string to filter details")
def do_mpcdf_build_status(self, subcmd, opts, *args):
    """mpcdf_build_status:
    List build statuses of a package for all software projects

    For a given package or the one from the current directory, iterate over all
    projects in "software" and the user's "home" project and display the build
    statuses of that package. Lines will be skipped if the status is "succeeded",
    "excluded", or "disabled". More statuses can be filtered out with --ignore.

    Usage:
        osc mpcdf_build_status [PACKAGE [PROJECT [REPOSITORIES]]]

    PACKAGE can be "all" to display the builds of all packages in the selected
    project. REPOSITORIES can be a single repository or a comma-separated list.

    Status explanation:
        disabled:     disabled in project or package meta data
        excluded:     disabled in the spec file
        broken:       invalid build setup
        unresolvable: dependency issue
        blocked:      waiting for other package to be built
        scheduled:    waiting to be built
        dispatching:  copying to the worker
        building:     build is running
        finished:     either succeeded or failed
        signing:      built successfully and waiting to be signed
        failed:       build error
        succeeded:    built successfully and ready to be installed
        deleting:     currenlty removing

    ${cmd_option_list}

    Examples:
       - Show the build statuses of the package 'foobar_1_0' in all software
         subprojects, but hide all 'unresolvable' builds:
         #> osc mpcdf_build_status foobar_1_0 --skip-home --ignore unresolvable
       - Show the build statuses of the package 'foobar_1_0' in a specific
         software subproject:
         #> osc mpcdf_build_status foobar_1_0 software:SLE_15:skylake
       - Print the build status of the package 'foobar_1_0' for a specific
         repository in a selected software subproject:
         #> osc mpcdf_build_status foobar_1_0 software:SLE_15:skylake openmpi_4_1_gcc_13
    """

    api_url = self.get_api_url()
    projects = []
    repository = None

    if len(args) == 0:
        if osc.core.is_package_dir(os.curdir):
            package = osc.core.store_read_package(os.curdir)
        else:
            raise osc.oscerr.WrongArgs("Specify PACKAGE or run command in an osc package checkout directory")

    elif len(args) == 1:
        package, = args
        if package == "all":
            raise osc.oscerr.WrongArgs("PACKAGE must not be 'all' if no specific project is selected")

    elif len(args) == 2:
        package, project = args
        projects = [project]

    elif len(args) == 3:
        package, project, repository = args
        projects = [project]

    else:
        raise osc.oscerr.WrongArgs("Invalid number of arguments")

    if len(projects) == 0:
        # collect what projects should be displayed
        visit_home_project = not opts.skip_home
        visit_software_project = True
        visit_software_subprojects = True
        try:
            enabled = mpcdf_common.get_attribute_values(api_url, osc.core.store_read_project(os.curdir), package, "MPCDF:enable_repositories")
            visit_software_project = "system" in enabled
            visit_software_subprojects = not visit_software_project
        except Exception:
            pass

        user = osc.conf.get_apiurl_usr(api_url)

        # fetch all projects from server
        url = osc.core.makeurl(api_url, ["source"])
        entries = ElementTree.fromstringlist(osc.core.streamfile(url, osc.core.http_GET))
        for entry in entries.findall("./entry"):
            project = entry.get("name")
            if (project.startswith("software:") and visit_software_subprojects) or \
                    (project == "software" and visit_software_project) or \
                    (project == "home:{0}".format(user) and visit_home_project):
                projects.append(project)

    colormap = dict(blocked="yellow",
                    broken="red",
                    building="blue",
                    deleting="blue",
                    disabled="green",
                    dispatching="blue",
                    failed="red",
                    finished="blue",
                    excluded="green",
                    locked="red",
                    scheduled="yellow",
                    signing="green",
                    succeeded="green",
                    unknown="red",
                    unresolvable="red")
    ordered_status = ["disabled", "excluded", "broken", "locked", "unresolvable", "blocked", "scheduled",
                      "dispatching", "building", "finished", "signing", "failed", "succeeded", "deleting", "unknown"]

    ignore_status = set(opts.ignore.split(",")) if opts.ignore is not None else set()
    ignore_status = ignore_status.union(["disabled", "excluded", "succeeded"])

    def print_stdout(text=""):
        if not opts.only_count:
            print(text)

    count = dict()
    for project in projects:
        print_stdout(project)

        package_arg = ""
        if package != "all":
            package_arg = "&package={0}".format(package)

        repository_arg = ""
        if repository is not None:
            repository_arg = "".join("&repository={0}".format(r) for r in repository.split(","))

        project_url = osc.core.makeurl(api_url, ["build", project, "_result"], query="view=status&multibuild=1{0}{1}".format(package_arg, repository_arg))

        try:
            resultlist = ElementTree.fromstringlist(osc.core.streamfile(project_url, osc.core.http_GET))
        except osc.core.HTTPError as e:
            if e.code == 404:
                print_stdout("   {0} {1}".format(package, colored("not found", "yellow")))
                print_stdout()
                continue
            else:
                raise e

        actualPackages = set()
        for result in resultlist.findall("./result"):
            for status in result.findall("./status"):
                actualPackages.add(status.get("package"))

        for pack in sorted(actualPackages):
            package_stdout = "   {0} (https://obs-api.mpcdf.mpg.de/package/show/{1}/{2})\n".format(pack, project, pack)

            codes = dict()
            details = dict()
            for result in resultlist.findall("./result"):
                for status in result.findall("./status"):
                    if status.get("package") == pack:
                        codes[result.get("repository")] = status.get("code")
                        if opts.details:
                            for detail in status.findall("./details"):
                                if opts.details == "*" or opts.details in detail.text:
                                    details[result.get("repository")] = detail.text

            any_printed = False
            for repo in sorted(codes.keys()):
                code = codes[repo]
                if code not in ignore_status or (repository is not None and opts.ignore is None):
                    # Only print if the status is not filtered out. In case the repository argument is present,
                    # we should always print the status, unless the has user explicitly filtered it with --ignore.
                    package_stdout += "      {0}: {1}\n".format(repo, colored(code, colormap.get(code, "red")))
                    any_printed = True
                    if repo in details:
                        package_stdout += "         {0}\n".format(details[repo])
                count[code] = count.get(code, 0) + 1

            if package != "all" or any_printed:
                print_stdout(package_stdout)
        print_stdout()

    if opts.count is not None or opts.only_count is not None:
        for code in ordered_status:
            if code in count:
                print("{0}: {1}".format(colored(code, colormap.get(code, "red")), count[code]))
        print("sum: {0}".format(sum(count.values())))
