import mpcdf_common

import osc
import osc.conf
import osc.core
import osc.cmdln


@osc.cmdln.option('-f', '--from', default="software",
                  help="Source project")
@osc.cmdln.option('-t', '--to',
                  help="Just sync with this single destination project")
@osc.cmdln.option('-p', '--private', action="store_true", default=False,
                  help="Do not copy over maintainer field (for test projects)")
@osc.cmdln.alias("mpcdf_sync_proj")
def do_mpcdf_sync_projects(self, subcmd, opts, *args):
    """mpcdf_sync_projects:
    Branch all/missing packages from a project to another and sync their metadata

    Usage:
        osc mpcdf_sync_projects
        osc mpcdf_sync_projects PACKAGE

    Examples:
        - Sync all new packages from software into all software: sub-projects
         #> osc mpcdf_sync_projects

        - Sync the package foobar_1_2 into all software: sub-projects
         #> osc mpcdf_sync_projects foobar_1_2

        - Update project software:SLE_12_SP3:skylake with all new packages from software
         #> osc mpcdf_sync_projects --to software:SLE_12_SP3:skylake

    ${cmd_option_list}
    """
    package = None

    if len(args) > 1:
        raise osc.oscerr.WrongArgs("Too many arguments")
    elif len(args) == 1:
        package, = args

    api_url = self.get_api_url()

    from_project = getattr(opts, "from") if getattr(opts, "from") else "software"

    if opts.to:
        to_projects = [opts.to]
    else:
        to_projects = None

    mpcdf_common.sync_projects(api_url, package,
                               from_project=from_project, to_projects=to_projects,
                               add_to_maintainers=not opts.private)
