from mpcdf_common import mpcdf_setup_subproject

import os
import osc
import osc.conf
import osc.core
import osc.cmdln


@osc.cmdln.option('-n', '--dry-run', action="store_true",
                  help="Do not actually run anything but output the resulting XML configuration")
@osc.cmdln.option('--diff', action="store_true",
                  help="Show (only) differences of prjconf and project meta")
@osc.cmdln.option('--only-project', action="store_true",
                  help="Only change project metadata, do not iterate over packages and change their enabled repositories")
@osc.cmdln.option('--remove-old', action="store_true", default=False,
                  help="Remove all obsolete repositories instead of only disabling builds for packages there")
@osc.cmdln.option('--remove-old-matching', default=None,
                  help="Remove all obsolete repositories matching this regular expression")
@osc.cmdln.alias("mpcdf_setup_sub")
def do_mpcdf_setup_subproject(self, subcmd, opts, *args):
    """mpcdf_setup_subproject:
    Setup a software: sub-project

    Given a list of compilers, MPI libraries, and possibly CUDA versions via
    the projects attributes, this command creates repositories for all the
    resulting combinations

    Additionally, the prjconf is set-up to enforce the microarchitecture via
    a "Constraint:" statement

    Usage:
        osc mpcdf_setup_subproject [software:OS:microarchitecture]

    ${cmd_option_list}
    """

    if len(args) == 0:
        if osc.core.is_project_dir(os.curdir) or osc.core.is_package_dir(os.curdir):
            project = osc.core.store_read_project(os.curdir)
        else:
            raise osc.oscerr.WrongArgs('Specify PROJECT or run command in an osc checkout directory')

    elif len(args) == 1:
        project, = args
    else:
        raise osc.oscerr.WrongArgs("Too many arguments")

    sw, distribution, microarchitecture = project.split(":")

    if sw != "software":
        raise osc.oscerr.WrongArgs('Given project is not below the "software" project')

    mpcdf_setup_subproject(self.get_api_url(),
                           project, distribution, microarchitecture,
                           dry_run=opts.dry_run, diff=opts.diff,
                           remove_old=opts.remove_old, remove_old_matching=opts.remove_old_matching,
                           only_project=opts.only_project)
