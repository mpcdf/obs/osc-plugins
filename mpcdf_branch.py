import mpcdf_common

import osc
import osc.conf
import osc.core
import osc.cmdln


def do_mpcdf_branch(self, subcmd, opts, *args):
    """mpcdf_branch:
    Branch package from software to your home project

    This creates a branch of the package PACKAGE in the central
    'software' repository into your home:$USER project, and sets
    all required meta-data for the enabled repositories.

    Usage:
        osc mpcdf_branch PACKAGE [home:$USER]

    ${cmd_option_list}
    """

    apiurl = self.get_api_url()

    if len(args) == 0:
        raise osc.oscerr.WrongArgs("Missing argument: PACKAGENAME")
    elif len(args) == 1:
        package, = args
        target_project = "home:" + osc.conf.get_apiurl_usr(apiurl)
    elif len(args) == 2:
        package, target_project = args
        if not target_project.startswith("home:"):  # + osc.conf.get_apiurl_usr(apiurl)):
            raise osc.oscerr.WrongArgs("Target must be a home project")
    if len(args) > 2:
        raise osc.oscerr.WrongArgs("Too many arguments")

    mpcdf_common.sync_projects(apiurl, package=package, from_project="software",
                               to_projects=(target_project,), add_to_maintainers=False)
