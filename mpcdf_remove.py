import os
import osc
import osc.conf
import osc.core
import osc.cmdln
import urllib


@osc.cmdln.option('--ignore-missing', action="store_true",
                  help="Ignore if package is missing in some/all sub-projects")
def do_mpcdf_remove(self, subcmd, opts, *args):
    """mpcdf_remove:
    Removes a package from all sub-projects of software:

    Usage:
        osc mpcdf_remove [PACKAGE]
    """

    if len(args) == 0:
        if osc.core.is_package_dir(os.curdir):
            package = osc.core.store_read_package(os.curdir)
        else:
            raise osc.oscerr.WrongArgs('Specify PACKAGE or run command in an osc checkout directory')

    elif len(args) == 1:
        package, = args
    else:
        raise osc.oscerr.WrongArgs("Too many arguments")

    api_url = self.get_api_url()

    projects = [p for p in osc.core.meta_get_project_list(api_url)
                if p.startswith("software:") and not (p == "software:dist" or p == "software:images")]

    for project in projects:
        print("Removing from", project, "...")
        try:
            osc.core.delete_package(api_url, project, package)
        except urllib.error.HTTPError as e:
            if e.code == 404 and opts.ignore_missing:
                print("  .. not present there!")
                continue
            else:
                raise

    print("\nThe package still exists in 'software', you can undo what you did with\n\n"
          "  osc mcpdf_push\n\n"
          "To fully delete the package remove it also from 'software', and possibly your\n"
          "home: project, using the web-inteface or 'osc rdelete'")
