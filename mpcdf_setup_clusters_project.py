import osc
import osc.conf
import osc.core
import osc.cmdln
from xml.etree import ElementTree

import mpcdf_common


@osc.cmdln.option('-n', '--dry-run', action="store_true",
                  help="Do not actually run anything but output the resulting prj/prjconf configuration")
@osc.cmdln.option('--diff', action="store_true",
                  help="Show (only) differences of prjconf and project meta")
@osc.cmdln.option('--remove-old', action="store_true", default=False,
                  help="Remove obsolete repositories")
@osc.cmdln.alias("mpcdf_setup_clusters")
def do_mpcdf_setup_clusters_project(self, subcmd, opts, *args):
    """mpcdf_setup_clusters_project:
    Set-up the 'clusters' project

    This creates the 'prjconf' for the 'clusters' project

    Usage:
        osc mpcdf_setup_clusters_project

    ${cmd_option_list}

    """

    if len(args) != 0:
        raise osc.oscerr.WrongArgs("Too many arguments")

    api_url = self.get_api_url()

    software_meta = mpcdf_common.project_meta(api_url, "software")
    images_meta = mpcdf_common.project_meta(api_url, "images:modules")
    subprojects = [p for p in osc.core.meta_get_project_list(api_url)
                   if p.startswith("software:")]

    macros = {}
    for subproject in subprojects:
        target = subproject[len("software:"):].replace(":", "-")

        if target in macros:
            raise Exception("Internal error")

        macros[target] = []
        for attribute in mpcdf_common.config_attributes:
            _, name = attribute.split(":")
            values = mpcdf_common.overloaded_project_attribute(api_url, subproject, attribute)

            # Fix naming mistake. # TODO: Remove after intel_2021_3_0 is gone
            if name == "compiler_modules":
                def fix(name):
                    if name == "intel_2021_3_0":
                        return "intel_21_3_0"
                    else:
                        return name
                values = map(fix, values)

            available = ",".join(sorted(values))
            if not available:
                available = "%{nil}"
            macros[target].append("%available_{0} {1}".format(name, available))

        all_compilers = mpcdf_common.overloaded_project_attribute(api_url, subproject, "MPCDF:compiler_modules")
        latest_intel = mpcdf_common.latest_package(c for c in all_compilers if c.startswith("intel"))
        latest_gcc = mpcdf_common.latest_package(c for c in all_compilers if c.startswith("gcc"))
        if latest_intel is not None:
            macros[target].append("%latest_intel " + latest_intel)
        if latest_gcc is not None:
            macros[target].append("%latest_gcc " + latest_gcc)

    orig_prjconf, prjconf_head, prjconf_ours, prjconf_tail = mpcdf_common.parse_prjconf(api_url, "clusters")

    prjconf = list(map(mpcdf_common.decode_it, osc.core.show_project_conf(api_url, "clusters")))

    prjconf_ours = [mpcdf_common.prjconf_start_marker]

    prj_meta = mpcdf_common.project_meta(api_url, "clusters")
    mpcdf_common.sort_repos(prj_meta)
    old_prj = ElementTree.tostring(prj_meta, encoding=osc.core.ET_ENCODING)

    if opts.remove_old:
        for oldrepo in prj_meta.findall("./repository"):
            prj_meta.remove(oldrepo)

    for subproject in subprojects:
        _, distribution, microarchitecture = subproject.split(":")
        reponame = "{0}-{1}".format(distribution, microarchitecture)
        prjconf_ours.append('%if "%_repository" == "{0}"'.format(reponame))
        prjconf_ours.append("Macros:")
        prjconf_ours.extend(macros[reponame])
        prjconf_ours.append("%distribution {0}".format(distribution))
        prjconf_ours.append(":Macros")
        prjconf_ours.append("%endif")
        prjconf_ours.append("")

        if not opts.remove_old:
            oldrepo = prj_meta.find("./repository[@name='{0}']".format(reponame))
            if oldrepo:
                prj_meta.remove(oldrepo)
            else:
                print("New section", reponame)

        r = ElementTree.SubElement(prj_meta, "repository")
        r.set("name", reponame)
        r.text = "\n    "

        image_repo = images_meta.find(f"./repository[@name='{distribution}']")
        if image_repo:
            p = ElementTree.SubElement(r, "path")
            p.set("project", images_meta.get("name"))
            p.set("repository", distribution)
            p.tail = "\n    "

        for repo in osc.core.get_repositories_of_project(api_url, subproject):
            p = ElementTree.SubElement(r, "path")
            p.set("project", subproject)
            p.set("repository", repo)
            p.tail = "\n    "

        for arch in software_meta.findall('./repository[@name="{0}"]/arch'.format(distribution)):
            a = ElementTree.SubElement(r, "arch")
            a.text = arch.text
            a.tail = "\n    "
        a.tail = "\n  "
        r.tail = "\n  "
    r.tail = "\n"

    prjconf_ours.append(mpcdf_common.prjconf_end_marker)
    new_prjconf = "".join(prjconf_head) + "\n".join(prjconf_ours) + "".join(prjconf_tail)

    mpcdf_common.sort_repos(prj_meta)
    new_prj = ElementTree.tostring(prj_meta, encoding=osc.core.ET_ENCODING)

    if opts.diff:
        mpcdf_common.stringdiff("".join(orig_prjconf), "old-prjconf", new_prjconf, "new-prjconf")
        mpcdf_common.stringdiff(old_prj + "\n", "old-prj-meta", new_prj + "\n", "new-prj-meta")

    if opts.dry_run:
        if not opts.diff:
            print("osc meta prjconf {0} -F - <<EOF\n{1}\nEOF\n".format("clusters", prjconf))
            print("osc meta prj {0} -F - <<EOF\n{1}\nEOF\n".format("clusters", new_prj))
    else:
        if new_prjconf == "".join(orig_prjconf):
            print("prjconf unchanged")
        else:
            print("Updating prjconf meta")
            osc.core.edit_meta("prjconf", "clusters", data=new_prjconf, apiurl=api_url)

        # Update repositories
        if new_prj == old_prj:
            print("prj meta unchanged")
        else:
            print("Updating prj meta")
            osc.core.edit_meta("prj", "clusters", data=new_prj, force=True, apiurl=api_url)
