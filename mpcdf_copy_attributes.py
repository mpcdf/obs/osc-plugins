import mpcdf_common

import osc
import osc.conf
import osc.core
import osc.cmdln


@osc.cmdln.option('-t', '--to-apiurl', metavar='URL',
                  help='URL of destination api server. Default is the source api server.')
@osc.cmdln.alias("mpcdf_copy_attrs")
def do_mpcdf_copy_attributes(self, subcmd, opts, *args):
    """mpcdf_copy_attributes:
    Copy attributes from one package to another

    Usage:
        osc mpcdf_copy_attributes SOURCE_PROJECT SOURCE_PACKAGE DEST_PROJECT DEST_PACKAGE

    ${cmd_option_list}
    """

    if len(args) != 4:
        raise osc.oscerr.WrongArgs('Wrong number of arguments')
    else:
        src_project, src_package, dst_project, dst_package = args

    src_api = self.get_api_url()
    if opts.to_apiurl:
        dst_api = osc.conf.config['apiurl_aliases'].get(opts.to_apiurl, opts.to_apiurl)
    else:
        dst_api = src_api

    for attribute in mpcdf_common.package_attributes + mpcdf_common.config_attributes:
        try:
            attr = mpcdf_common.get_attribute(src_api, src_project, src_package, attribute)
        except mpcdf_common.UnsetAttributeException:
            if mpcdf_common.has_attribute(dst_api, dst_project, dst_package, attribute):
                mpcdf_common.remove_attribute(dst_api, dst_project, dst_package, attribute)
            continue
        mpcdf_common.set_attribute(dst_api, dst_project, dst_package, attr)
