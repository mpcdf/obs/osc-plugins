import mpcdf_common

import os
import osc
import osc.conf
import osc.core
import osc.cmdln


@osc.cmdln.option('-n', '--dry-run', action="store_true",
                  help="Do not actually perform any changes but print what would be stored")
@osc.cmdln.option('--recreate', action="store_true",
                  help="Re-create the set of enabled repositories from the stored attributes on the server")
@osc.cmdln.option('--compiler-modules',
                  help="Restrict the set of compilers to use")
@osc.cmdln.option('--mpi-modules',
                  help="Restrict the set of MPI implementations to use")
@osc.cmdln.option('--cuda-modules',
                  help="Restrict the set of CUDA implementations to use")
@osc.cmdln.option('--rocm-modules',
                  help="Restrict the set of ROCm implementations to use")
@osc.cmdln.option('--pgi-modules',
                  help="Restrict the set of PGI compilers to use")
@osc.cmdln.option('--amd-modules',
                  help="Restrict the set of AMD compilers to use")
@osc.cmdln.option('--set', metavar="FLAGS",
                  help="Modify the set of enabled repositories, without this the current setup is displayed. "
                       "FLAGS is a comma-separated list of a subset of 'system', 'compilers', 'mpi', 'cuda', 'cuda_mpi', 'rocm', 'rocm_mpi', 'pgi', 'pgi_mpi', 'amd', 'amd_mpi'")
@osc.cmdln.option('--disable', action="store_true",
                  help="Disable building this package")
@osc.cmdln.alias("mpcdf_enable_repos")
def do_mpcdf_enable_repositories(self, subcmd, opts, *args):
    """mpcdf_enable_repositories:
    Select all appropriate repositories for an MPCDF package

    Due to the large number of repository combinations at MPCDF it is
    cumbersome to enable all the appropriate repositories for a given package
    by hand.

    This command allows you to set the <enable/> flags for certain kinds of
    repositories at once.

    Without --set this command only displays the current configuration.

    It is possible to combine this with user-defined <disabled/> flags that
    override the settings of this command, for example to disable the build of
    the package with a certain repository from a given set (e.g. one
    troublesome compiler)

    Usage:
        osc mpcdf_enable_repositories                       [[PROJECT] PACKAGE]
        osc mpcdf_enable_repositories --set FLAGS [options] [[PROJECT] PACKAGE]
        osc mpcdf_enable_repositories --recreate            [[PROJECT] PACKAGE]
        osc mpcdf_enable_repositories --disable             [[PROJECT] PACKAGE]

    ${cmd_option_list}
    """

    if len(args) == 0:
        if osc.core.is_package_dir(os.curdir):
            package = osc.core.store_read_package(os.curdir)
            project = osc.core.store_read_project(os.curdir)
        else:
            raise osc.oscerr.WrongArgs('Specify PACKAGE or run command in an osc package checkout directory')

    elif len(args) == 1:
        package, = args
        project = osc.core.store_read_project(os.curdir)

    elif len(args) == 2:
        project, package = args
    else:
        raise osc.oscerr.WrongArgs("Too many arguments")

    if project.startswith("images"):
        raise osc.oscerr.WrongArgs("This function is not available for packages in 'images'")

    api_url = self.get_api_url()

    if opts.disable:
        mpcdf_common.set_attribute_values(api_url, project, package, "MPCDF:enable_repositories", "")
    elif opts.set:
        mpcdf_common.set_attribute_values(api_url, project, package, "MPCDF:enable_repositories", opts.set.split(","))

        def set_or_remove(flag, attribute_name):
            if flag is not None:
                print("Setting attribute", attribute_name, "to", flag)
                mpcdf_common.set_attribute_values(api_url, project, package, attribute_name, flag.split(","))
            elif mpcdf_common.has_attribute(api_url, project, package, attribute_name):
                print("Removing attribute", attribute_name, "from package")
                mpcdf_common.remove_attribute(api_url, project, package, attribute_name)

        set_or_remove(opts.compiler_modules, "MPCDF:compiler_modules")
        set_or_remove(opts.mpi_modules, "MPCDF:mpi_modules")
        set_or_remove(opts.cuda_modules, "MPCDF:cuda_modules")
        set_or_remove(opts.rocm_modules, "MPCDF:rocm_modules")
        set_or_remove(opts.pgi_modules, "MPCDF:pgi_modules")
        set_or_remove(opts.amd_modules, "MPCDF:amd_modules")

    if opts.recreate or opts.set or opts.disable:
        mpcdf_common.mpcdf_enable_repositories(api_url, project, package, verbose=True, dry_run=opts.dry_run)
        enabled = mpcdf_common.get_attribute_values(api_url, project, package, "MPCDF:enable_repositories")
        if project == "software":
            mpcdf_common.sync_projects(api_url, package, verbose=True)

    elif (opts.compiler_modules or opts.mpi_modules or opts.cuda_modules or opts.rocm_modules):
        print("ERROR: Invalid arguments, try --help")

    else:
        try:
            enabled = mpcdf_common.get_attribute_values(api_url, project, package, "MPCDF:enable_repositories")
        except Exception:
            enabled = ()
            print("ERRROR: No attribute MPCDF:enable_repositories present, package unmanaged")
        if enabled:
            def print_attr(description, attribute_name):
                try:
                    values = mpcdf_common.get_attribute_values(api_url, project, package, attribute_name)
                except Exception:
                    return
                print(description, ",".join(values) if values else "<empty list>")

            print("Enabled for:", ",".join(enabled))
            print_attr("- Subset of compiler modules set to:", "MPCDF:compiler_modules")
            print_attr("- Subset of MPI modules set to:", "MPCDF:mpi_modules")
            print_attr("- Subset of CUDA modules set to:", "MPCDF:cuda_modules")
            print_attr("- Subset of ROCm modules set to:", "MPCDF:rocm_modules")
            print_attr("- Subset of PGI modules set to:", "MPCDF:pgi_modules")
            print_attr("- Subset of AMD modules set to:", "MPCDF:amd_modules")
        else:
            print("Disabled")
