from functools import reduce

import mpcdf_common

import os
import osc
import osc.conf
import osc.core
import osc.cmdln


@osc.cmdln.option('-d', '--details', action="store_true",
                  help='Print details about each package as well')
def do_mpcdf_info(self, subcmd, opts, *args) -> None:
    """mpcdf_info:
    Basic information about an MPCDF OBS project

    Usage:
        osc mpcdf_info [PROJECT]

    ${cmd_option_list}
    """

    if len(args) == 0:
        if osc.core.is_project_dir(os.curdir) or osc.core.is_package_dir(os.curdir):
            project = osc.core.store_read_project(os.curdir)
        else:
            raise osc.oscerr.WrongArgs('Specify PROJECT or run command in an osc checkout directory')

    elif len(args) == 1:
        project, = args
    else:
        raise osc.oscerr.WrongArgs("Too many arguments")

    print("Project `{0}`\n".format(project))

    api_url = self.get_api_url()

    def print_attribute(description, attribute):
        if mpcdf_common.has_attribute(api_url, project, None, attribute):
            values = list(sorted(mpcdf_common.get_attribute_values(api_url, project, None, attribute)))
            print(" {0}:\n   ".format(description) + "\n   ".join(values))
            print()

    print_attribute("Compilers", "MPCDF:compiler_modules")
    print_attribute("MPI libraries", "MPCDF:mpi_modules")
    print_attribute("CUDA versions", "MPCDF:cuda_modules")
    print_attribute("ROCm versions", "MPCDF:rocm_modules")
    print_attribute("PGI compilers", "MPCDF:pgi_modules")
    print_attribute("AMD compilers", "MPCDF:amd_modules")

    if not opts.details:
        raise SystemExit(0)

    unmanaged = []

    packages = osc.core.meta_get_packagelist(api_url, project)

    pkg_name_width = reduce(max, (len(p) for p in packages))
    pkg_name_width = max(pkg_name_width, len("Package"))

    pkg_name_fmt = "{{:{0}}}".format(pkg_name_width)

    print(" " + pkg_name_fmt.format("Package"), "Enabled repositories")
    print(" " + "-" * (pkg_name_width), "-" * len("Enabled repositories"))

    for package in packages:
        if mpcdf_common.has_attribute(api_url, project, package, "MPCDF:enable_repositories"):
            enabled_repos = mpcdf_common.get_attribute_values(api_url, project, package, "MPCDF:enable_repositories")
        else:
            unmanaged.append(package)
            continue

        def subset(description, attribute):
            if mpcdf_common.has_attribute(api_url, project, package, attribute):
                return " {0}={1}".format(description, ",".join(mpcdf_common.get_attribute_values(api_url, project, package, attribute)))
            return ""

        print(" " + pkg_name_fmt.format(package),
              ("--set=" + (",".join(enabled_repos))) if enabled_repos else "--disable",
              subset("--compiler-modules", "MPCDF:compiler_modules")
              + subset("--mpi-modules", "MPCDF:mpi_modules")
              + subset("--cuda-modules", "MPCDF:cuda_modules")
              + subset("--rocm-modules", "MPCDF:rocm_modules"))
    print()

    if unmanaged:
        print(" Unmanaged packages")
        print(" " + "-" * (1 + pkg_name_width))
        for package in unmanaged:
            print(" " + package)
        print()
