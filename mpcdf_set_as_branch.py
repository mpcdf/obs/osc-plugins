import mpcdf_common

import os
import osc
import osc.conf
import osc.core
import osc.cmdln


@osc.cmdln.alias("mpcdf_setbranch")
def do_mpcdf_set_as_branch(self, subcmd, opts, *args):
    """mpcdf_set_as_branch:
    Convert a package to be a branch of another

    The package MY_PACKAGE is set-up to be a branch of the package
    MAIN_PACKAGE

    The currently checked out package is used as MY_PACKAGE, if that
    argument is omitted, and MAIN_PACKAGE is assumed to have the same name as
    MY_PACKAGE, of omitted.

    Usage:
        osc mpcdf_set_as_branch MAIN_PROJECT [MAIN_PACKAGE]
        osc mpcdf_set_as_branch MY_PROJECT MY_PACKAGE MAIN_PROJECT [MAIN_PACKAGE]

    ${cmd_option_list}
    """
    if len(args) < 1:
        raise osc.oscerr.WrongArgs("Not enough arguments")
    if len(args) == 1:
        main_project, = args
        main_package = None
    if len(args) == 2:
        main_project, main_package = args
    if len(args) in [1, 2]:
        if osc.core.is_package_dir(os.curdir):
            my_package = osc.core.store_read_package(os.curdir)
            my_project = osc.core.store_read_project(os.curdir)
        else:
            raise osc.oscerr.WrongArgs('Specify MY_PROJECT and MY_PACKAGE or run command in an osc package checkout directory')
        if main_package is None:
            main_package = my_package

    if len(args) == 3:
        my_project, my_package, main_project = args
        main_package = my_package
    if len(args) == 4:
        my_project, my_package, main_project, main_package = args
    elif len(args) > 4:
        raise osc.oscerr.WrongArgs("Too many arguments")

    if not mpcdf_common.set_as_branch(self.get_api_url(), my_project, my_package, main_project, main_package):
        raise SystemExit(1)
