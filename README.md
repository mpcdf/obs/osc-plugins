MPCDF-specific OSC plugins
==========================

Installation
------------

Put the `.py` files into `~/.osc-plugins` to make them available for the osc command,
for example by

```
$> git clone https://gitlab.mpcdf.mpg.de/mpcdf/obs/osc-plugins ~/.osc-plugins
```

Background
----------

* http://mpcdf.pages.mpcdf.de/obs/manual/manual.pdf
* https://en.opensuse.org/openSUSE:OSC_plugins

