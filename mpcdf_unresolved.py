import os
import sys
import osc
import osc.conf
import osc.core
import osc.cmdln

from xml.etree import ElementTree
from urllib.parse import quote


@osc.cmdln.option('-r', '--repo', metavar='REPO',
                  help='specify repository')
def do_mpcdf_unresolved(self, subcmd, opts, *args):
    """mpcdf_unresolved:
    List unresolved RPM dependencies of built binaries

    Usage:
        osc mpcdf_unresolved [PACKAGE [PROJECT]]

    ${cmd_option_list}
    """

    apiurl = self.get_api_url()

    if len(args) == 0:
        if osc.core.is_package_dir(os.curdir):
            project = osc.core.store_read_project(os.curdir)
            package = osc.core.store_read_package(os.curdir)
        else:
            raise osc.oscerr.WrongArgs('Specify PACKAGE or run command in an osc package checkout directory')

    elif len(args) == 1:
        project = osc.core.store_read_project(os.curdir)
        package, = args

    elif len(args) == 2:
        project, package = args
    else:
        raise osc.oscerr.WrongArgs("Too many arguments")

    if opts.repo is not None:
        repos = [repo for repo in osc.core.get_repos_of_project(apiurl, project) if repo.name == opts.repo]
        indent = ""
    else:
        repos = osc.core.get_repos_of_project(apiurl, project)
        indent = "  "

    results = []
    for repo in repos:
        results.append((repo, osc.core.get_binarylist(apiurl, project, repo.name, repo.arch, package=package)))

    found = False
    for repo, files in results:
        unresolved = []
        for file in files:
            if not file.endswith(".rpm"):
                continue

            if os.isatty(sys.stdout.fileno()):
                string = project + " / " + package + " / " + repo.name + " / " + repo.arch + " / " + file
                sys.stdout.write('\x1b[2K')
                print(string[:os.get_terminal_size().columns], end="\r", flush=True)

            unresolved.append((file, []))
            url = apiurl + "/build/" + project + "/" + quote(repo.name) + "/" + \
                quote(repo.arch) + "/" + quote(package) + "/" + quote(file) + "?view=fileinfo_ext"
            res = osc.core.http_request("GET", url)
            root = ElementTree.parse(res)
            for requires in root.findall("./requires_ext"):
                if len(requires) == 0:
                    unresolved[-1][1].append(requires.attrib["dep"])

        if sum(len(u[1]) for u in unresolved) > 0:
            found = True
            if os.isatty(sys.stdout.fileno()):
                sys.stdout.write('\x1b[2K')

            if len(results) > 1:
                print("{0} ({1})".format(repo.name, repo.arch))

            for file, deps in unresolved:
                if len(deps) > 0:
                    print(indent + "{0}:".format(file))
                    for dep in deps:
                        print(indent + "  " + dep)
                    print()

    if not found:
        if os.isatty(sys.stdout.fileno()):
            sys.stdout.write('\x1b[2K')
        print("No unresolved dependencies found")
