from mpcdf_common import mpcdf_setup_subproject, project_meta, decode_it, default_microarch

import os
import osc
import osc.conf
import osc.core
import osc.cmdln


@osc.cmdln.option('-n', '--dry-run', action="store_true",
                  help="Do not actually run anything but output the resulting XML configuration")
@osc.cmdln.option('--diff', action="store_true",
                  help="Show (only) differences of prjconf and project meta")
@osc.cmdln.option('--remove-old', action="store_true", default=False,
                  help="Remove all obsolete repositories instead of only disabling builds for packages there")
@osc.cmdln.option('--remove-old-matching', default=None,
                  help="Remove all obsolete repositories matching this regular expression")
@osc.cmdln.option('--only-project', action="store_true",
                  help="Only change project metadata, do not iterate over packages and change their enabled repositories")
@osc.cmdln.option('--distribution', metavar="DIST",
                  help="Base distribution, necessary argument unless set previously for this project. "
                       "Valid arguments are the names of any repository in the 'software' project.")
@osc.cmdln.option('--microarchitecture', metavar="ARCH",
                  help="Configure project to use ARCH as microarchitecture, "
                       "defaults to '{0}' or what has been set before in the "
                       "home project".format(default_microarch))
@osc.cmdln.alias("mpcdf_setup_home")
def do_mpcdf_setup_home_project(self, subcmd, opts, *args):
    """mpcdf_setup_home_project:
    Setup a home project based on a software: sub-project

    Set-up a home:$USER project for building packages for the 'software'
    project hierarchy (i.e. application group packages)

    Usage:
        osc mpcdf_setup_home_project [home:USER]

    ${cmd_option_list}
    """

    if len(args) == 0:
        if osc.core.is_project_dir(os.curdir) or osc.core.is_package_dir(os.curdir):
            project = osc.core.store_read_project(os.curdir)
        else:
            raise osc.oscerr.WrongArgs('Specify PROJECT or run command in an osc checkout directory')

    elif len(args) == 1:
        project, = args
    else:
        raise osc.oscerr.WrongArgs("Too many arguments")

    if project.split(":")[0] != "home":
        raise osc.oscerr.WrongArgs('Given project is not a home: project')

    api_url = self.get_api_url()

    if opts.distribution is None:
        # Get existing value from project meta
        dist_repo = project_meta(api_url, project).find(
            "./repository[@name='System']/path")
        if dist_repo is not None:
            distribution = dist_repo.get("repository")
        else:
            raise osc.oscerr.WrongArgs('Could not determine desired distribution, please specify --distribution explicitly')
    else:
        distribution = opts.distribution

    microarchitecture = None
    if opts.microarchitecture is None:
        for prjconfline in map(decode_it, osc.core.show_project_conf(api_url, project)):
            if prjconfline.startswith("Constraint: hostlabel"):
                microarchitecture = prjconfline.split()[2]
                break
    else:
        microarchitecture = opts.microarchitecture

    if microarchitecture is None:
        microarchitecture = default_microarch
        print("Setting microarchitecture to '{0}', by default".format(default_microarch))

    parent = "software:{0}:{1}".format(distribution, microarchitecture)

    mpcdf_setup_subproject(api_url,
                           project, distribution, microarchitecture,
                           parent=parent,
                           dry_run=opts.dry_run, diff=opts.diff,
                           remove_old=opts.remove_old, remove_old_matching=opts.remove_old_matching,
                           only_project=opts.only_project)
