#!/usr/bin/python3

import osc
import osc.conf
import osc.core

import json
import requests
import time
try:
    from termcolor import colored
except ImportError:
    def colored(text, *args):
        return text
from xml.etree import ElementTree


def do_mpcdf_admin_info(self, subcmd, opts, *args):
    """mpcdf_admin_info:
    Print some status information about the OBS:

    - The number or currently waiting build jobs and the number of
      active workers for the different architectures
    - The failed builds of the cluster packages
    - The failed builds of the common module packages
    - The cluster update status

    Usage:
        osc mpcdf_admin_info

    ${cmd_option_list}
    """

    api_url = self.get_api_url()

    print_workers(api_url)
    print_common_modules(api_url)
    print_cluster_packages(api_url)
    print_cluster_updates("https://obs.mpcdf.mpg.de/clusters/")


def print_workers(api_url):
    print("Workers:")

    url = osc.core.makeurl(api_url, ["worker", "_status"])
    entries = ElementTree.fromstringlist(osc.core.streamfile(url, osc.core.http_GET))

    for waiting in entries.findall("./waiting"):
        if waiting.get("arch") == "x86_64":
            waiting_jobs = int(waiting.get("jobs"))
            plural = ""
            if waiting_jobs != 1:
                plural = "s"
            print("   {0} waiting build job{1}".format(waiting_jobs, plural))

    building_workers = {}
    for building in entries.findall("./building"):
        arch = building.get("workerid").split("-")[0]
        if arch not in building_workers:
            building_workers[arch] = 1
        else:
            building_workers[arch] += 1

    for arch in sorted(building_workers.keys()):
        print("   {0}: {1} building".format(arch, building_workers[arch]))

    if len(building_workers) == 0 and waiting_jobs > 0:
        print("   {0}".format(colored("all idling, but waiting build jobs exist", "red")))

    down_workers = 0
    for down in entries.findall("./down"):
        down_workers += 1
        print("   {0}: {1}".format(down.get("workerid").split(":")[0], colored("down", "red")))

    if len(building_workers) == 0 and waiting_jobs == 0 and down_workers == 0:
        print("   {0}".format(colored("all idling", "green")))


def print_cluster_packages(api_url):
    print("Cluster packages:")
    colormap = dict(unresolvable="red", failed="red", broken="red", building="blue", blocked="yellow", finished="blue", scheduled="yellow", signing="green")

    url = osc.core.makeurl(api_url, ["build", "clusters", "_result"], query=dict(view="status"))
    entries = ElementTree.fromstringlist(osc.core.streamfile(url, osc.core.http_GET))

    all_succeeded = True
    for result in entries.findall("./result"):
        for status in result.findall("./status"):
            cluster = status.get("package")
            code = status.get("code")
            if cluster[0].isupper() and code not in ["disabled", "excluded", "succeeded"]:
                print("   {0}: {1}".format(cluster, colored(code, colormap.get(code, "red"))))
                all_succeeded = False
    if all_succeeded:
        print("   {0}".format(colored("all succeeded", "green")))


def print_cluster_updates(url):
    print("Cluster updates:")

    clusterlist = json.loads(requests.get(url + "clusterlist").text)

    all_succeeded = True
    for cluster, host in clusterlist.items():
        status = json.loads(requests.get(url + "statefiles/" + host).text)
        if status["error"]:
            print("   {0}: {1}".format(cluster, colored("error", "red")))
            all_succeeded = False
        elif status["time"] < time.time() - 86400:
            print("   {0}: {1}".format(cluster, colored("outdated", "red")))
            all_succeeded = False
    if all_succeeded:
        print("   {0}".format(colored("all succeeded", "green")))


def print_common_modules(api_url):
    print("Common modules:")
    colormap = dict(unresolvable="red", failed="red", broken="red", building="blue", blocked="yellow", scheduled="yellow")

    url = osc.core.makeurl(api_url, ["build", "clusters", "_result"], query=dict(view="status"))
    entries = ElementTree.fromstringlist(osc.core.streamfile(url, osc.core.http_GET))

    errors = {}
    for result in entries.findall("./result"):
        repo = result.get("repository")
        for status in result.findall("./status"):
            common_module = status.get("package")
            code = status.get("code")
            if common_module.islower() and code not in ["disabled", "excluded", "succeeded"]:
                if common_module in errors:
                    errors[common_module].append((repo, code))
                else:
                    errors[common_module] = [(repo, code)]
    if len(errors) > 0:
        for common_module, infos in errors.items():
            print("   {0} (https://obs-api.mpcdf.mpg.de/package/show/clusters/{1}):".format(common_module, common_module))
            for info in infos:
                print("      {0}: {1}".format(info[0], colored(info[1], colormap.get(info[1], "red"))))
    else:
        print("   {0}".format(colored("all succeeded", "green")))
